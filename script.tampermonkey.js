// ==UserScript==
// @name         FZUtility v3
// @namespace    https://nvx0.codes/
// @version      v3
// @description  h
// @author       nvx0 /w fixes by coolian
// @match        https://factorio.zone/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=factorio.zone
// @grant      GM_addStyle
// @grant unsafeWindow
// ==/UserScript==

GM_addStyle ( `
body {
   margin-top: -300px
}

.control-container {
   max-width: 1630px
}

:root {
    --background-div-color: rgb(41, 41, 41);
    --background-div-border: #353535;
    --background-div-border-hover: #a5a5a5;
    --max-content-width: 300px;
    --blue: #0d6efd;
    --indigo: #6610f2;
    --purple: #6f42c1;
    --pink: #d63384;
    --red: #dc3545;
    --orange: #fd7e14;
    --yellow: #ffc107;
    --green: #198754;
    --teal: #20c997;
    --cyan: #0dcaf0;
    --gray: #adb5bd;
    --black: #000;
    --white: #fff;
    background-color: #333;
    color: white
}

@keyframes init{from {transform: scale(0.998);}to {transform: scale(1);}}

* {
    user-select: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    -webkit-touch-callout: none;
    -webkit-tap-highlight-color: transparent;
    -webkit-font-smoothing: antialiased;
    scroll-behavior: smooth;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    -webkit-overflow-scrolling: touch;
    position: relative;
    animation: init .25s;
}

/* Font sizing */
.font-weight-9 { font-weight: 900; }
.font-weight-8 { font-weight: 800; }
.font-weight-7 { font-weight: 700; }
.font-weight-6 { font-weight: 600; }
.font-weight-5 { font-weight: 500; }
.font-weight-4 { font-weight: 400; }
.font-weight-3 { font-weight: 300; }
.font-weight-2 { font-weight: 200; }

/* ContentBox */

.ContentBox h1 { max-width: var(--max-content-width) }
.ContentBox h2 { max-width: var(--max-content-width) }
.ContentBox h3 { max-width: var(--max-content-width) }
.ContentBox h4 { max-width: var(--max-content-width) }
.ContentBox h5 { max-width: var(--max-content-width) }
.ContentBox h6 { max-width: var(--max-content-width) }
.ContentBox p { max-width: var(--max-content-width) }
.ContentBox a { max-width: var(--max-content-width) }

/* [contentbox] Limitations bypass */

.noLimit h1 { top: 5px; max-width: 100%; }
.noLimit h2 { top: 5px;  max-width: 100%; }
.noLimit h3 { top: 5px;  max-width: 100%; }
.noLimit h4 { top: 5px; max-width: 100%; }
.noLimit h5 { top: 5px; max-width: 100%; }
.noLimit h6 { top: 5px; max-width: 100%; }
.noLimit p { top: 5px; max-width: 100%; }
.noLimit a { top: 5px; max-width: 100%; }

.ContentBox {
    margin-top: -7px;
    border: 1px solid var(--background-div-border);
    background-color: var(--background-div-color);
    border-radius: 5px;
    max-height: fit-content;
    max-width: fit-content;
    color: white;
    padding-left: 2%;
    padding-right: 2%;
    padding-bottom: 25px;
    transition: ease 0.2s;
    margin-left: auto;
    z-index: 99999;
}

.ContentBox:hover {
    border: 1px solid var(--background-div-border-hover);
    transition: ease 0.2s;
}

.ContentBoxPush {
    margin-top: -15px;
}

.ContentBoxPushS {
    margin-top: 3px;
}


/* Buttons */

.ButtonBase {
    border: 1px solid var(--white);
    color: var(--white);
    background-color: transparent;
    border-radius: 3px;
    padding-left: 30px;
    padding-right: 30px;
    padding-top: 7.5px;
    padding-bottom: 7.5px;
    font-weight: 700;
    font-size: 17px;
    transition: ease 0.2s;
    margin: 5px;
    cursor: pointer;
}

.ButtonBase:hover {
    border: 1px solid white;
    color: black;
    background-color: white;
    transition: ease 0.2s;
}

.btn-red {
    color: var(--red);
    border-color: var(--red);
}

.btn-red:hover {
    background-color: var(--red);
    border-color: var(--red);
    color:white
}

.btn-blue {
    color: var(--blue);
    border-color: var(--blue);
}

.btn-blue:hover {
    background-color: var(--blue);
    border-color: var(--blue);
    color:white
}

.btn-green {
    color: var(--green);
    border-color: var(--green);
}

.btn-green:hover {
    background-color: var(--green);
    border-color: var(--green);
    color:white
}

.btn-yellow {
    color: var(--yellow);
    border-color: var(--yellow);
}

.btn-yellow:hover {
    background-color: var(--yellow);
    border-color: var(--yellow);
    color:black
}

.btn-purple {
    color: var(--purple);
    border-color: var(--purple);
}

.btn-purple:hover {
    background-color: var(--purple);
    border-color: var(--purple);
    color:white
}

.btn-orange {
    color: var(--orange);
    border-color: var(--orange);
}

.btn-orange:hover {
    background-color: var(--orange);
    border-color: var(--orange);
    color:white
}

/* Inputs */

.input-area {
    border: 1px solid var(--background-div-border);
    background-color: var(--background-div-color);
    border-radius: 5px;
    width: 99%;
    padding: 3px;
    transition: ease 0.2s;
    cursor: pointer;
    color:white
}

.input-area::placeholder {
    padding-left: 10px;
    color: var(--gray);
    cursor: none;
}

.input-area:hover {
    transition: ease 0.2s;
    border-color: var(--white);
}

.input-area:focus {
    border-color: var(--blue);
    transition: ease 0.2s;
    color: white;
    caret-color: transparent;
}

.input-area::content {
    color:white
}

.si {
   top: -5px;
   padding: 3px;
}

.control-container {
  margin-top: 9px;
  background-color: #444;
  border-radius: 10px;
  bottom: 10px;
}

.output-area {
  background-color: #222;
  border-radius: 5px;
  font-size: 15px;
  overflow-x: hidden;
}
.output-log {
  color: white;
  font-size: 15px;
}
a {
  color: var(--purple)
}
` );

(function() {
function getVisitSecret(){let e=[],t=[];for(let s=0;unsafeWindow.console.logs.length>s;s++)e.push(unsafeWindow.console.logs[s].visitSecret??void 0);return(e.forEach(e=>{void 0!==e&&t.push(e)}),t===[])?"backend has not connected yet.":t.toString()}function broadcast(e){let t=document.getElementById("output-area"),s=document.createElement("div");s.setAttribute("class","output-info"),s.innerHTML=e,t.append(s)}unsafeWindow.console.stdlog=unsafeWindow.console.log.bind(console),unsafeWindow.console.logs=[],unsafeWindow.console.log=(...e)=>{unsafeWindow.console.stdlog||(console.stdlog=console.log,unsafeWindow.console.stdlog=unsafeWindow.console.log),unsafeWindow.console.logs.push(...e),unsafeWindow.console.stdlog(...e)};const controlContainer=document.querySelector("body");let vs=-1;setTimeout(function(){let e=getVisitSecret();document.getElementById("visitSecret").innerHTML="Visit secret: "+e,localStorage.setItem("vs0",e)},5e3),localStorage.getItem("lastSave")?controlContainer.insertAdjacentHTML("beforebegin",`
        <div style='position:relative;top:16px' class='ContentBox noLimit'>
            <a>&nbsp;</a>
            <h3 id='modsX'>FZUtility v3<h3/>
            <h5 class='ContentBoxPush'>by nvx0 /w fixes by coolian + help from Ebaw<h5/>
            <h1>${(localStorage.lastSave.charAt(0).toUpperCase()+localStorage.lastSave.toString().replace("s","").split("t")[0]+"t "+localStorage.lastSave.split("t")[1])??"factorio.zone"}</h1>
                <p class='ContentBoxPush' id='visitSecret'>Visit secret: ...</p>
                <p class='ContentBoxPush'>Session secret: ${localStorage.userToken}</p>
                <h3>Upload mods</h3>
                <input type="file" id='files' name="filefield" multiple="multiple">
           <br><br>
        </div>
    `):controlContainer.insertAdjacentHTML("beforebegin",`
        <div style='position:relative;top:16px' class='ContentBox noLimit'>
            <a>&nbsp;</a>
            <h3 id='modsX'>FZUtility v3<h3/>
            <h5 class='ContentBoxPush'>by nvx0 /w fixes by coolian + help from Ebaw<h5/>
            <h1>factorio.zone</h1>
                <p class='ContentBoxPush' id='visitSecret'>Visit secret: ...</p>
                <p class='ContentBoxPush'>Session secret: ${localStorage.userToken??"N/A"}</p>
                <h3>Upload mods</h3>
                <input type="file" name="filefield" multiple="multiple">
           <br><br>
        </div>
    `),document.getElementById("files").onchange=function(){0===document.getElementById("files").files.length&&alert("Select some mods =[");let e=0;for(let t=0;t<document.getElementById("files").files.length;t++){document.getElementById("files").files.item(t),document.getElementById("txt");var s=new FormData;s.append("file",document.getElementById("files").files[t]),s.append("size",document.getElementById("files").files[t].size),s.append("visitSecret",localStorage.getItem("vs0")),fetch("https://factorio.zone/api/mod/upload",{method:"POST",body:s}).then(()=>{e++,document.getElementById("modsX").innerHTML=`FZUtility v3 [${e}/${document.getElementById("files").files.length}]`}).catch(t=>{document.getElementById("modsX").innerHTML=`FZUtility v3 [${e}/${document.getElementById("files").files.length}]`})}},document.getElementById("start-button").classList.remove("pure-button"),document.getElementById("start-button").classList.remove("start-button"),document.getElementById("start-button").classList.add("ButtonBase"),document.getElementById("start-button").classList.add("btn-green"),document.getElementById("socket-info").classList.add("input-area"),document.getElementById("socket-info").classList.add("si"),document.getElementById("stop-button").classList.remove("pure-button"),document.getElementById("stop-button").classList.remove("stop-button"),document.getElementById("stop-button").classList.add("ButtonBase"),document.getElementById("stop-button").classList.add("btn-red"),document.getElementById("saves").setAttribute("class","input-area ContentBoxPushS"),document.getElementById("versions").setAttribute("class","input-area ContentBoxPushS"),document.getElementById("regions").setAttribute("class","input-area ContentBoxPushS");
})();
