# FZ Utility

# How to?
That also applies to any updates =)
<br>
1. Download this repo (click on the ZIP text, selected by default)
<br><br>
<img src="https://cdn.discordapp.com/attachments/824325869900267563/1053766825223127140/image.png">
<br>
2. Save it in correct place (make sure to remember where have you put it)
<br><br>
<img src="https://media.discordapp.net/attachments/824325869900267563/1053767524992426104/image.png">
<br>
3. Open it via WinRar or 7ZIP
<br><br>
<img src="https://media.discordapp.net/attachments/824325869900267563/1053768619026305164/image.png">
<br>
4. Open TamperMonkey in your browser 
<br><br>
<img src="https://media.discordapp.net/attachments/824325869900267563/1053768876703359006/image.png">
<br>
5. Unpack the file into desired location
<br><br>
<img src="https://media.discordapp.net/attachments/824325869900267563/1053769464451182592/image.png">
<br>
<br>
6. Drag file from your location to TamperMonkey website.
<br><br>
7. Click the "Install" button
<br><br>
<img src="https://media.discordapp.net/attachments/824325869900267563/1053769529722949662/image.png">
<br><br>
Congratulations! You have installed the extansion.

## Information
This is a (official) repost of original repo's that were under those accounts on GitHub:
```
1. nvx0
2. tonawetnieja
```

## Why reposted?
As you may know from factorio.zone server my both accounts got flagged for no reason, so why this repo exists.

## Contact 
If you encountered any bugs or you want to talk about other things, you can contact me on:
```
1. Discord: maly wiosek#4412 (ID: 824243749105565716)
2. Telegram: https://t.me/cloudzik76
```

## Contributions
All countributions are welcome!

## Donations
I am very thankful for any donations, it really keeps me going 
```
Bitcoin: bc1q234slrrnq34hjx4ju66j0ahnlkpmv3gf54d9hn
Ethereum: 0x73bef2f1a0Dd557fadE901C8dBCe8ad2e74F2747
Litecoin: ltc1qskphvgqsnd8r429egmcu3c4r7dc6atghhqyf7e
Tron: TUmyE97Mdrvos9RsRpUPBVtXEp47KQ19PP
Dash: XuxKkokLzPJevx7nvACgsUT4t7R1NBmGwX
USDT (TRC-20): TUmyE97Mdrvos9RsRpUPBVtXEp47KQ19PP
USDT (ERC-20): 0x73bef2f1a0Dd557fadE901C8dBCe8ad2e74F2747
Doge: DHhHhJ7ub8PsBQqb8rRFCEvVaFKaRCn7HK
Solana: EvrHX9tjwBoscifUG4yGWPxuLouBwgfXvU8nDaHA8dP3
Nano: nano_3minnje1w1kjh7nxcg3fmqrzz5hunu3cemgirk6gkqc41jryc4pmaj83drwq
USDC (ERC-20): 0x73bef2f1a0Dd557fadE901C8dBCe8ad2e74F2747
```
